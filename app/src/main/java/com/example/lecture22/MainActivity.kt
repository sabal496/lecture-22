package com.example.lecture22

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       init()
    }

    private fun init(){
        addfragment(SignInFragment(),R.id.signinfragment,"signinfragment")
        registerbtn.setOnClickListener(){
            addfragment(registerfragment(),R.id.registerfragment,"registerfragment")
        }


    }

    private fun addfragment(frag :Fragment,conteiner:Int,tag:String){
        val transaction = supportFragmentManager.beginTransaction()
        val fragment=frag
        transaction.add(conteiner,fragment,tag)
        transaction.addToBackStack(tag)
        transaction.commit()

    }
}
