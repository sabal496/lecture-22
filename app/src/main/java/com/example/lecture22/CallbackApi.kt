package com.example.lecture22

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}