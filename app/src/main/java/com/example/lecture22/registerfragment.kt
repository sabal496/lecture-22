package com.example.lecture22

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_registerfragment.*
import kotlinx.android.synthetic.main.fragment_registerfragment.view.*


import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [registerfragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class registerfragment : Fragment() {

    lateinit var itemview:View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        itemview=inflater.inflate(R.layout.fragment_registerfragment, container, false)
        init()
        return itemview
    }

    private  fun init(){
        itemview.regbtn.setOnClickListener(){
            val email=username.text.toString()
            val password=password.text.toString()
            val confpassword=confirmpassword.text.toString()
            if(email.isNotEmpty() && password.isNotEmpty() && password==confpassword){
                val map= mutableMapOf<String,String>()
                map["email"]=email
                map["password"]=password

                RequestApi.posRequest(
                    "register",
                    object : CallbackApi {
                        override fun onResponse(value: String?) {
                            if (value != "null") {
                                val token = JSONObject(value)
                                if (token.has("token"))
                                    if (token.getString("token") == "QpwL5tke4Pnpja7X4")
                                        Toast.makeText(
                                            itemview.context,
                                            "ok",
                                            Toast.LENGTH_SHORT
                                        ).show()

                            }
                        }

                        override fun onFailure(value: String?) {
                        }

                    },
                    map
                )
            }

        }
    }

}
