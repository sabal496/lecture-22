package com.example.lecture22

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object RequestApi {

    var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()


    var service = retrofit.create(
        Apiservice::class.java)

    fun getRequest(path :String,callback: CallbackApi){
        val call= service.Getrequest(path)
        call.enqueue(calll(callback))

    }

    fun posRequest(path :String, callback: CallbackApi, params:MutableMap<String,String>){
        val call=
            service.PostRequest(path,params)
        call.enqueue(calll(callback))

    }


    private fun calll(callback: CallbackApi) =  object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())

        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            callback.onResponse(response.body().toString())

        }

    }



    interface Apiservice {
        @GET("{path}")
        fun Getrequest(@Path("path") path: String): Call<String>

        @FormUrlEncoded
        @POST("{path}")
        fun PostRequest(@Path("path")path: String,@FieldMap parameters:Map<String,String>):Call<String>
    }



}