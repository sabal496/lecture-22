package com.example.lecture22

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*


import org.json.JSONObject


class SignInFragment : Fragment() {
    lateinit var itemview:View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        itemview=inflater.inflate(R.layout.fragment_sign_in, container, false)
        init()
        return itemview
    }
    private fun init(){


        itemview.signinbtn.setOnClickListener(){
            val username=username.text.toString()
            val password=password.text.toString()
            if(username.isNotEmpty() && password.isNotEmpty()){
                val map= mutableMapOf<String,String>()
                map["email"]=username
                map["password"]=password
                RequestApi.posRequest(
                    "login",
                    object : CallbackApi {
                        override fun onResponse(value: String?) {
                            Log.d("reg", value)
                            if (value == "null") {
                                Toast.makeText(
                                    itemview.context,
                                    "password or email is incorrect",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                val token = JSONObject(value)
                                if (token.has("token"))
                                    if (token.getString("token") == "QpwL5tke4Pnpja7X4"){
                                        Toast.makeText(
                                            itemview.context,
                                            "sign ined succsesfully",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }


                            }
                        }

                        override fun onFailure(value: String?) {

                        }
                    },
                    map
                )

            }
        }
    }

}
